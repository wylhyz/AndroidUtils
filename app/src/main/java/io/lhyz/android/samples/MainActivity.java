package io.lhyz.android.samples;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import io.lhyz.android.utils.asserts.Asserts;
import io.lhyz.android.utils.info.AppUtils;
import io.lhyz.android.utils.info.DeviceUtils;
import io.lhyz.android.utils.logger.LogHelper;
import io.lhyz.android.utils.network.ConnectivityUtils;
import io.lhyz.android.utils.storage.StorageUtils;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = LogHelper.makeTag(MainActivity.class);

    TextView state;
    TextView path;
    TextView app;
    TextView device;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Asserts.checkNotNull(findViewById(R.id.check_state)).setOnClickListener(this);
        Asserts.checkNotNull(findViewById(R.id.check_path)).setOnClickListener(this);
        Asserts.checkNotNull(findViewById(R.id.check_app)).setOnClickListener(this);
        Asserts.checkNotNull(findViewById(R.id.check_device)).setOnClickListener(this);

        state = (TextView) findViewById(R.id.state);
        Asserts.checkNotNull(state);
        path = (TextView) findViewById(R.id.path);
        Asserts.checkNotNull(path);
        app = (TextView) findViewById(R.id.app);
        Asserts.checkNotNull(app);
        device = (TextView) findViewById(R.id.device);
        Asserts.checkNotNull(device);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.check_state:
                int state_net = ConnectivityUtils.testConnectivityManager(this);
                switch (state_net) {
                    case ConnectivityUtils.STATE_NOT_AVAILABLE:
                        state.setText("STATE_NOT_AVAILABLE");
                        break;
                    case ConnectivityUtils.STATE_GPRS:
                        state.setText("STATE_GPRS");
                        break;
                    case ConnectivityUtils.STATE_WIFI:
                        state.setText("STATE_WIFI");
                        break;
                    case ConnectivityUtils.STATE_UNKNOWN:
                        state.setText("STATE_UNKNOWN");
                        break;
                    default:
                        break;
                }
                break;
            case R.id.check_path:
                path.setText(StorageUtils.getPrimaryStoragePath(this) + "\n" + StorageUtils.getSecondaryStoragePath(this));
                break;
            case R.id.check_app:
                app.setText(AppUtils.getAppName(this) + "\n" + AppUtils.getPackageName(this));
                break;
            case R.id.check_device:
                device.setText(DeviceUtils.getUserAgent());
                break;
        }
    }
}
