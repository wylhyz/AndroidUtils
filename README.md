# Android Utils

[ ![Download](https://api.bintray.com/packages/wylhyz/maven/android-utils/images/download.svg) ](https://bintray.com/wylhyz/maven/android-utils/_latestVersion)
[![Build Status](https://travis-ci.org/wylhyz/AndroidUtils.svg?branch=master)](https://travis-ci.org/wylhyz/AndroidUtils)

一些常用的工具类集合


使用方法

project -> build.gradle
```gradle
allprojects {
    repositories {
        jcenter()

        maven {
            url 'https://dl.bintray.com/wylhyz/maven'
        }
    }
}
```

然后添加下面到项目module的build.gradle下
```gradle
compile 'io.lhyz.android:utils:0.1.0'
```