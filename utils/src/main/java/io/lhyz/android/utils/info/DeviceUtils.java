package io.lhyz.android.utils.info;

import android.os.Build;

/**
 * 获取与设备相关的信息
 */
@SuppressWarnings("unused")
public class DeviceUtils {

    /**
     * 获取设备型号
     *
     * @return 设备型号字符串（比如Google Nexus 5 - 5.0.0 - API 21 - 1080x1920）
     */
    public static String getDeviceName() {
        return Build.MODEL;
    }

    /**
     * 获取设备的android版本
     *
     * @return android版本号（比如5.0）
     */
    public static String getDeviceVersion() {
        return Build.VERSION.RELEASE;
    }

    /**
     * 获取一个用于网络请求的userAgent，根据是设备名和系统版本
     *
     * @return userAgent（类似Google Nexus 5 - 5.0.0 - API 21 - 1080x1920/5.0）
     */
    public static String getUserAgent() {
        return getDeviceName() + "/" + getDeviceVersion();
    }
}
