package io.lhyz.android.utils.measure;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.WindowManager;

/**
 * 测量工具
 */
public class MeasureUtils {

    /**
     * 获取屏幕尺寸类
     *
     * @param context 上下文对象
     * @return 屏幕显示测量对象
     */
    public static DisplayMetrics getScreenMetrics(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);
        return dm;
    }

    /**
     * 将dp转换成实际的px
     *
     * @param context 上下文
     * @param dp      dp
     * @return 像素px
     */
    public static float dp2px(Context context, float dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                context.getResources().getDisplayMetrics());
    }
}
