package io.lhyz.android.utils.storage;

import android.content.Context;
import android.os.storage.StorageManager;

import java.lang.reflect.Method;

/**
 * 使用反射的方式获取storage manager的隐藏方法
 */
@SuppressWarnings("unused")
public class StorageUtils {

    /**
     * 获取主要外部存储（机身虚拟SD存储）
     *
     * @return 路径 或 ""
     */
    public static String getPrimaryStoragePath(Context context) {
        StorageManager storageManager = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
        try {
            Method getVolumePathsMethod = StorageManager.class.getMethod("getVolumePaths");
            String[] paths = (String[]) getVolumePathsMethod.invoke(storageManager);
            if (paths.length > 0) {
                return paths[0];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取外部 SD 存储器（可插拔SD卡）
     *
     * @return 外置SD卡路径 或 ""
     */
    public static String getSecondaryStoragePath(Context context) {
        StorageManager storageManager = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
        try {
            Method getVolumePathsMethod = StorageManager.class.getMethod("getVolumePaths");
            String[] paths = (String[]) getVolumePathsMethod.invoke(storageManager);
            if (paths.length > 1) {
                return paths[1];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取存储状态
     *
     * @param path 使用上述方法获取到的路径
     * @return 如果 path 可用就返回 {Environment.MEDIA_MOUNTED }
     */
    public static String getStorageState(Context context, String path) {
        String state = "unknown";
        StorageManager storageManager = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
        try {
            Method getVolumeStateMethod = StorageManager.class.getMethod("getVolumeState", String.class);
            state = (String) getVolumeStateMethod.invoke(storageManager, path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return state;
    }
}
