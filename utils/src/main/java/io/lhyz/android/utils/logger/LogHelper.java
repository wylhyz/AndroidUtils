package io.lhyz.android.utils.logger;

/**
 * 帮助打log
 */
@SuppressWarnings("unused")
public class LogHelper {
    /**
     * 传入需要生成TAG的类
     *
     * @param clazz 类名.class
     * @return 类名的完整签名（包括包名）
     */
    public static String makeTag(Class<?> clazz) {
        return clazz.getCanonicalName();
    }
}
