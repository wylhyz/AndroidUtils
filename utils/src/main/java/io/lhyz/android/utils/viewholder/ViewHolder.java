package io.lhyz.android.utils.viewholder;

import android.util.SparseArray;
import android.view.View;

/**
 * Created by lhyz on 2016/3/29.
 * <p>
 * generic view holder
 */
@SuppressWarnings("unused")
public class ViewHolder {

    @SuppressWarnings("unchecked")
    public static <T> T get(View view, int id) {
        SparseArray<View> viewHolder = (SparseArray) view.getTag();
        if (viewHolder == null) {
            viewHolder = new SparseArray();
            view.setTag(viewHolder);
        }
        View childView = viewHolder.get(id);
        if (childView == null) {
            childView = view.findViewById(id);
            viewHolder.put(id, childView);
        }

        return (T) childView;
    }
}
