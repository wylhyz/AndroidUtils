package io.lhyz.android.utils.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * this class require the permission <em>android.permission.ACCESS_NETWORK_STATE</em>
 * <p/>
 * 需要权限 <em>android.permission.ACCESS_NETWORK_STATE</em>
 * <p/>
 * 主要功能是测试当前网络状态，注意：如果同时打开了wifi和gprs，也只是按照当前连接到了什么网络判断，比如默认开了wifi和gprs，
 * 但是wifi优先级肯定是高于gprs的，所以会优先连接wifi，所以查询的gprs状态也只能是disconnected
 */
@SuppressWarnings("unused")
public class ConnectivityUtils {

    public static final int STATE_NOT_AVAILABLE = 0x0;
    public static final int STATE_GPRS = 0x1;
    public static final int STATE_WIFI = 0x2;
    public static final int STATE_UNKNOWN = 0x3;

    public static int testConnectivityManager(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        //检查是否网络连接可用 method 1
        if (networkInfo == null) {
            return STATE_NOT_AVAILABLE;
        }

        //检查是否网络连接可用 method 2
        boolean available = networkInfo.isAvailable();
        if (available) {
            //gprs connectivity checked
            NetworkInfo.State state = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState();
            if (NetworkInfo.State.CONNECTED == state) {
                return STATE_GPRS;
            }

            //wifi connectivity checked
            state = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState();
            if (NetworkInfo.State.CONNECTED == state) {
                return STATE_WIFI;
            }
        } else {
            return STATE_NOT_AVAILABLE;
        }

        //如果可用但不是上述提到的gprs或是wifi，就返回unknown
        return STATE_UNKNOWN;

        /**
         * if we has no network connected, we may want to open the setting activity
         */
//        context.startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
//        context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
    }

    /**
     * 测试是否wifi环境
     *
     * @param context 上下文环境
     * @return 是否wifi状态
     */
    public static boolean isWifi(Context context) {
        return testConnectivityManager(context) == STATE_WIFI;
    }

    /**
     * 测试是否移动网络环境
     *
     * @param context 上下文环境
     * @return 是否gprs状态
     */
    public static boolean isGPRS(Context context) {
        return testConnectivityManager(context) == STATE_GPRS;
    }

    /**
     * 判断网络是否可用
     *
     * @param context 上下文环境
     * @return 网络是否可用
     */
    public static boolean isAvailable(Context context) {
        int state = testConnectivityManager(context);
        return state != STATE_NOT_AVAILABLE && state != STATE_UNKNOWN;
    }
}
